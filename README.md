# AndroidTechChallenge

Lalamove Mobile Developer Technical Challenge

Google Maps:
* I acquired an Google Maps API key for Android from the Google Cloud Platform.
* After acquiring the API key, I added an element in the AndroidManifest.xml for the API key.
* In the build.gradle, I added a dependency (implementagition 'com.google.android.gms:play-services-maps:12.0.1') Google Play Services for maps.
* Used a MapView to display the Google Maps in the map_layout.xml.