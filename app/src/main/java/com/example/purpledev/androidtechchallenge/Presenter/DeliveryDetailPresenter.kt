package com.example.purpledev.androidtechchallenge.Presenter

import com.example.purpledev.androidtechchallenge.Model.DeliveryListModel

/**
 * Created by Christine on 8/12/2019.
 */

interface DeliveryDetailPresenter {

    fun onDeliveryListEvent()

}
