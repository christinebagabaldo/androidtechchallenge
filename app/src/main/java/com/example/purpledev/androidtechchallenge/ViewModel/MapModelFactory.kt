package com.example.purpledev.androidtechchallenge.ViewModel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

import com.example.purpledev.androidtechchallenge.Model.DeliveryListModel

import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton


/**
 * Created by Christine on 8/15/2019.
 */

class MapModelFactory @Inject
constructor(private val mProviderMap: Map<Class<out ViewModel>, Provider<ViewModel>>) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return mProviderMap[modelClass]?.get() as T
    }
}
