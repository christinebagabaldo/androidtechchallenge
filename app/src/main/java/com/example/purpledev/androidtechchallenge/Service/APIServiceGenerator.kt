package com.example.purpledev.androidtechchallenge.Service

import java.util.concurrent.TimeUnit


import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object APIServiceGenerator {

    private val API_BASE_URL = "https://mock-api-mobile.dev.lalamove.com"
    private var retrofit: Retrofit? = null


    val client: Retrofit?
        get() {

            retrofit = Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit as Retrofit?
        }

}
