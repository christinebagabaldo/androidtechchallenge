package com.example.purpledev.androidtechchallenge.ViewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.BindingAdapter

import com.example.purpledev.androidtechchallenge.Model.DeliveryListModel
import com.example.purpledev.androidtechchallenge.Model.Location
import com.example.purpledev.androidtechchallenge.Repositories.DeliveriesRepository
import com.squareup.picasso.Picasso

import java.util.ArrayList

import de.hdodenhof.circleimageview.CircleImageView


class DeliveryListViewModel : ViewModel {

    lateinit var description: String
    lateinit var imageUrl: String
    lateinit var location: Location
    lateinit var id: String

    var arrayListMutableLiveData = MutableLiveData<ArrayList<DeliveryListViewModel>>()

    private var deliveriesAPI: DeliveriesRepository? = null

    constructor() {

        deliveriesAPI = DeliveriesRepository()
        arrayListMutableLiveData = DeliveriesRepository.getArrayListMutableLiveData(deliveriesAPI!!)

    }

    constructor(deliveryListModel: DeliveryListModel) {
        this.description = deliveryListModel.description + " at " + deliveryListModel.location?.address
        this.id = deliveryListModel.id
        this.imageUrl = deliveryListModel.imageUrl
        this.location = deliveryListModel.location!!
    }

    companion object {

        @BindingAdapter("bind:imageUrl")
        @JvmStatic
        fun loadImage(imageView: CircleImageView, imageUrl: String) {
            Picasso.get().load(imageUrl).fit().into(imageView)
        }
    }

}
