package com.example.purpledev.androidtechchallenge.Module

import android.arch.lifecycle.ViewModel

import com.example.purpledev.androidtechchallenge.Model.DeliveryListModel
import com.example.purpledev.androidtechchallenge.ViewModel.MapModelFactory
import com.example.purpledev.androidtechchallenge.ViewModel.MapViewModel

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

import javax.inject.Provider

import dagger.MapKey
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass


/**
 * Created by Christine on 8/15/2019.
 */
@Module
class ViewModelModule {
    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

    @Provides
    internal fun viewModelFactory(providerMap: Map<Class<out ViewModel>, Provider<ViewModel>>): MapModelFactory {
        return MapModelFactory(providerMap)
    }


    @Provides
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    internal fun viewModel1(deliveryListModel: DeliveryListModel): ViewModel {
        return MapViewModel(deliveryListModel)
    }
}
