package com.example.purpledev.androidtechchallenge.Activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.widget.Toast

import com.example.purpledev.androidtechchallenge.Adapter.DeliveryListAdapter
import com.example.purpledev.androidtechchallenge.Model.DeliveryListModel
import com.example.purpledev.androidtechchallenge.R
import com.example.purpledev.androidtechchallenge.ViewModel.MapModelFactory
import com.example.purpledev.androidtechchallenge.ViewModel.MapViewModel


import com.example.purpledev.androidtechchallenge.databinding.MapLayoutBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

import javax.inject.Inject

class MapActivity : FragmentActivity() {

    var mapViewModel: MapViewModel? = null
    internal var deliveryListAdapter: DeliveryListAdapter? = null
    internal var dataBindingUtil: DataBindingUtil? = null
    internal lateinit var mapLayoutBinding: MapLayoutBinding
    @Inject internal var mapModelFactory: MapModelFactory? = null

    public override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)

        mapViewModel = ViewModelProviders.of(this, mapModelFactory).get(MapViewModel::class.java)
        mapLayoutBinding = DataBindingUtil.setContentView(this, R.layout.map_layout)
        mapViewModel?.setData()
        mapLayoutBinding.mapContainerView = mapViewModel


    }

}
