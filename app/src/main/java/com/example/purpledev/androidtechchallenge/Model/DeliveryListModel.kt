package com.example.purpledev.androidtechchallenge.Model

import com.google.gson.annotations.SerializedName

import de.hdodenhof.circleimageview.CircleImageView

class DeliveryListModel {

    @SerializedName("description")
     var description: String = ""
    @SerializedName("imageUrl")
     var imageUrl: String = ""
    @SerializedName("location")
    var location: Location? = null
    @SerializedName("id")
     var id: String = ""

    constructor(description: String, imageUrl: String, location: Location, id: String) {
        this.description = description
        this.imageUrl = imageUrl
        this.location = location
        this.id = id
    }

    constructor() {

    }
}
