package com.example.purpledev.androidtechchallenge.Adapter

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

import com.example.purpledev.androidtechchallenge.Activity.MapActivity
import com.example.purpledev.androidtechchallenge.Model.DeliveryListModel
import com.example.purpledev.androidtechchallenge.Model.Location
import com.example.purpledev.androidtechchallenge.Presenter.DeliveryDetailPresenter
import com.example.purpledev.androidtechchallenge.R
import com.example.purpledev.androidtechchallenge.ViewModel.DeliveryListViewModel
import com.example.purpledev.androidtechchallenge.ViewModel.MapViewModel
import com.example.purpledev.androidtechchallenge.databinding.DeliveryDetailsContainerBinding
import com.squareup.picasso.Picasso

import java.io.Serializable

import de.hdodenhof.circleimageview.CircleImageView

class DeliveryListAdapter : RecyclerView.Adapter<DeliveryListAdapter.DeliveryListHolder> {

    var mapMutableLiveData = MutableLiveData<DeliveryListModel>()
    lateinit var deliveryListModel: DeliveryListModel

    private lateinit var deliveryListModelList: List<DeliveryListViewModel>
    private var layoutInflater: LayoutInflater? = null
    internal lateinit var context: Context

    constructor(deliveryListModels: List<DeliveryListViewModel>, context: Context) {
        this.context = context
        this.deliveryListModelList = deliveryListModels
    }

    constructor() {

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): DeliveryListHolder {


        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(viewGroup.context)
        }

        val deliveryDetailsContainerBinding = DataBindingUtil.inflate<DeliveryDetailsContainerBinding>(layoutInflater!!, R.layout.delivery_details_container, viewGroup, false)

        deliveryDetailsContainerBinding.presenter = object : DeliveryDetailPresenter {
            internal lateinit var mapViewModel: MapViewModel
            override fun onDeliveryListEvent() {
                val description = deliveryDetailsContainerBinding.containerView?.description
                val imageUrl = deliveryDetailsContainerBinding.containerView?.imageUrl
                val id = deliveryDetailsContainerBinding.containerView?.id
                val location = deliveryDetailsContainerBinding.containerView?.location
                deliveryListModel = DeliveryListModel(description.toString(), imageUrl.toString(), location!!, id.toString())
                mapViewModel = MapViewModel(deliveryListModel)

                Log.e("Data", deliveryListModel.description)

                val intent = Intent(context, MapActivity::class.java)
                context.startActivity(intent)

            }
        }

        return DeliveryListHolder(deliveryDetailsContainerBinding)

    }

    override fun onBindViewHolder(deliveryListHolder: DeliveryListHolder, i: Int) {

        val deliveryListViewModel = deliveryListModelList[i]

        deliveryListHolder.bind(deliveryListViewModel)

    }

    override fun getItemCount(): Int {
        return deliveryListModelList.size
    }

    inner class DeliveryListHolder(val deliveryDetailsContainerBinding: DeliveryDetailsContainerBinding) : RecyclerView.ViewHolder(deliveryDetailsContainerBinding.root) {

        fun bind(deliveryListViewModel: DeliveryListViewModel) {
            this.deliveryDetailsContainerBinding.containerView = deliveryListViewModel
            deliveryDetailsContainerBinding.executePendingBindings()
        }
    }

    interface OnDetailListener {
        fun onDetailListener(position: Int)
    }
}
