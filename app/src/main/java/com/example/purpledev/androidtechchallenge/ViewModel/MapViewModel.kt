package com.example.purpledev.androidtechchallenge.ViewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.databinding.BindingAdapter
import android.databinding.ObservableField
import android.os.Bundle
import android.util.Log
import android.widget.ImageView

import com.example.purpledev.androidtechchallenge.Activity.MapActivity
import com.example.purpledev.androidtechchallenge.Adapter.DeliveryListAdapter
import com.example.purpledev.androidtechchallenge.Model.DeliveryListModel
import com.example.purpledev.androidtechchallenge.Model.Location
import com.example.purpledev.androidtechchallenge.Presenter.DeliveryDetailPresenter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso

import javax.inject.Inject

/**
 * Created by Christine on 8/12/2019.
 */

class MapViewModel : ViewModel {
    lateinit var description: String
    lateinit var imageUrl: String
    var location: Location? = null
    var id: String? = null
    var mMapLatLng = ObservableField<LatLng>()

    companion object {
        @JvmStatic lateinit var deliveryListModel: DeliveryListModel

        @BindingAdapter("bind:imageUrl")
        @JvmStatic
        fun loadImage(imageView: ImageView, imageUrl: String) {

                Picasso.get().load(imageUrl).fit().into(imageView)

        }

        @BindingAdapter("initMap")
        @JvmStatic
        fun initMap(mapView: MapView?, latLng: LatLng) {

            if (mapView != null) {
                mapView.onCreate(Bundle())
                mapView.getMapAsync { googleMap ->
                    // Add a marker
                    googleMap.addMarker(MarkerOptions().position(latLng))
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17f))
                    mapView.onResume()
                }
            }

        }
    }


    constructor() {

    }

    @Inject
    constructor(mDeliveryListModel: DeliveryListModel) {

        deliveryListModel = mDeliveryListModel

    }

    fun setData() {

        this.description = deliveryListModel.description
        this.imageUrl = deliveryListModel.imageUrl
        val latLng = ObservableField<LatLng>()
        val myLatLng = LatLng(deliveryListModel.location?.lat!!, deliveryListModel.location?.lng!!)
        latLng.set(myLatLng)
        mMapLatLng = latLng

    }


}
