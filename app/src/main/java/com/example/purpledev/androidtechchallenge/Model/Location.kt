package com.example.purpledev.androidtechchallenge.Model

import com.google.gson.annotations.SerializedName

class Location(@field:SerializedName("address")
               var address: String, @field:SerializedName("lng")
               var lng: Double, @field:SerializedName("lat")
               var lat: Double)
