package com.example.purpledev.androidtechchallenge.Service

import com.example.purpledev.androidtechchallenge.Model.DeliveryListModel

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface APIServices {


    @get:GET("/deliveries")
    val deliveryList: Call<List<DeliveryListModel>>

}
