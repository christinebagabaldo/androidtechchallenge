package com.example.purpledev.androidtechchallenge.Activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar

import com.example.purpledev.androidtechchallenge.Adapter.DeliveryListAdapter

import com.example.purpledev.androidtechchallenge.R
import com.example.purpledev.androidtechchallenge.Repositories.DeliveriesRepository
import com.example.purpledev.androidtechchallenge.ViewModel.DeliveryListViewModel


import java.util.ArrayList

class DashboardActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    internal lateinit var deliveryListAdapter: DeliveryListAdapter
    private var deliveryListViewModel: DeliveryListViewModel? = null
    internal lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dashboard_activity_layout)
        progressBar = findViewById(R.id.progressBar)
        progressBar.visibility = View.VISIBLE

        recyclerView = findViewById(R.id.rv_delivery_list)
        deliveryListViewModel = ViewModelProviders.of(this).get(DeliveryListViewModel::class.java)

        deliveryListViewModel!!.arrayListMutableLiveData.observe(this, Observer { deliveryListViewModels ->
            progressBar.visibility = View.GONE
            deliveryListAdapter = DeliveryListAdapter(deliveryListViewModels!!, this@DashboardActivity)
            recyclerView!!.layoutManager = LinearLayoutManager(this@DashboardActivity)
            deliveryListAdapter.notifyDataSetChanged()
            recyclerView!!.adapter = deliveryListAdapter
        })
    }


}
