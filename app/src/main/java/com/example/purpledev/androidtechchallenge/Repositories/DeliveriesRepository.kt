package com.example.purpledev.androidtechchallenge.Repositories

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.util.Log
import android.widget.Toast


import com.example.purpledev.androidtechchallenge.Activity.MapActivity
import com.example.purpledev.androidtechchallenge.Adapter.DeliveryListAdapter
import com.example.purpledev.androidtechchallenge.Model.DeliveryListModel
import com.example.purpledev.androidtechchallenge.Service.APIServiceGenerator
import com.example.purpledev.androidtechchallenge.Service.APIServices
import com.example.purpledev.androidtechchallenge.ViewModel.DeliveryListViewModel


import java.util.ArrayList

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DeliveriesRepository {


    var arrayListMutableLiveData = MutableLiveData<ArrayList<DeliveryListViewModel>>()
    private var arrayList: ArrayList<DeliveryListViewModel>? = null
    private var deliveriesList: List<DeliveryListModel>? = null

    companion object {
        fun getArrayListMutableLiveData(deliveriesRepository: DeliveriesRepository): MutableLiveData<ArrayList<DeliveryListViewModel>> {

            val apiServices = APIServiceGenerator.client?.create(APIServices::class.java)
            val call = apiServices?.deliveryList
            call?.enqueue(object : Callback<List<DeliveryListModel>> {
                override fun onResponse(call: Call<List<DeliveryListModel>>, response: Response<List<DeliveryListModel>>) {
                     Log.e("JSON", response.body()!!.toString())
                    deliveriesRepository.deliveriesList = ArrayList(response.body()!!)
                    var deliveryListModel: DeliveryListModel
                    var deliveryListViewModel: DeliveryListViewModel

                    deliveriesRepository.arrayList = ArrayList()

                    for (i in deliveriesRepository.deliveriesList!!.indices) {
                        deliveryListModel = DeliveryListModel(deliveriesRepository.deliveriesList!![i].description, deliveriesRepository.deliveriesList!![i].imageUrl, deliveriesRepository.deliveriesList!![i].location!!, deliveriesRepository.deliveriesList!![i].id)
                        deliveryListViewModel = DeliveryListViewModel(deliveryListModel)
                        deliveriesRepository.arrayList!!.add(deliveryListViewModel)
                    }

                    deliveriesRepository.arrayListMutableLiveData.value = deliveriesRepository.arrayList

                }

                override fun onFailure(call: Call<List<DeliveryListModel>>, t: Throwable) {

                }
            })

            return deliveriesRepository.arrayListMutableLiveData
        }
    }

}
